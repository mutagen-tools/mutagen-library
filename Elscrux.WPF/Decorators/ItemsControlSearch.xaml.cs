﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
namespace Elscrux.WPF.Decorators;

public partial class ItemsControlSearch {
    public static readonly DependencyProperty ItemsControlProperty = DependencyProperty.Register(nameof(ItemsControl), typeof(ItemsControl), typeof(ItemsControlSearch), new PropertyMetadata());
    
    public ItemsControl ItemsControl {
        get => (ItemsControl) GetValue(ItemsControlProperty);
        set => SetValue(ItemsControlProperty, value);
    }
    
    public char SearchWordSeparator { get; set; } = '*';

    public event StringFilterEventHandler? Filter;

    public ItemsControlSearch() {
        InitializeComponent();
    }

    private void Search_TextChanged(object sender, TextChangedEventArgs e) {
        if (ItemsControl.ItemsSource == null) return;
        
        var sourceRecordList = (CollectionView) CollectionViewSource.GetDefaultView(ItemsControl.ItemsSource);
        sourceRecordList.Filter = ItemFilter;
    }
    
    private bool ItemFilter(object item) {
        if (string.IsNullOrEmpty(SearchInput.Text)) return true;
        
        var searchWords = SearchInput.Text.Split(SearchWordSeparator);
        
        foreach (var searchWord in searchWords) {
            var arguments = new StringFilterEventArgs(item, searchWord);
            Filter?.Invoke(this, arguments);
            
            if (!arguments.Accepted) return false;
        }

        return true;
    }
}