﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;
namespace Elscrux.WPF.Decorators.Behaviors;

public class RegExTextBoxBehavior : Behavior<TextBox> {
    public static readonly DependencyProperty RegularExpressionProperty = DependencyProperty.Register(nameof(RegularExpression), typeof(string), typeof(RegExTextBoxBehavior), new FrameworkPropertyMetadata(".*"));
    public static readonly DependencyProperty MaxLengthProperty = DependencyProperty.Register(nameof(MaxLength), typeof(int), typeof(RegExTextBoxBehavior), new FrameworkPropertyMetadata(int.MinValue));
    
    public string RegularExpression {
        get => (string) GetValue(RegularExpressionProperty);
        set => SetValue(RegularExpressionProperty, value);
    }

    public int MaxLength {
        get => (int) GetValue(MaxLengthProperty);
        set => SetValue(MaxLengthProperty, value);
    }

    protected override void OnAttached() {
        base.OnAttached();
        AssociatedObject.PreviewTextInput += OnPreviewTextInput;
        DataObject.AddPastingHandler(AssociatedObject, OnPaste);
    }

    private void OnPaste(object sender, DataObjectPastingEventArgs e) {
        if (e.DataObject.GetDataPresent(DataFormats.Text)) {
            var text = Convert.ToString(e.DataObject.GetData(DataFormats.Text)) ?? string.Empty;

            if (!IsValid(text, true)) {
                e.CancelCommand();
            }
        } else {
            e.CancelCommand();
        }
    }

    private void OnPreviewTextInput(object sender, TextCompositionEventArgs e) {
        e.Handled = !IsValid(e.Text, false);
    }

    protected override void OnDetaching() {
        base.OnDetaching();
        AssociatedObject.PreviewTextInput -= OnPreviewTextInput;
        DataObject.RemovePastingHandler(AssociatedObject, OnPaste);
    }

    private bool IsValid(string newText, bool paste) {
        return !ExceedsMaxLength(newText, paste) && Regex.IsMatch(newText, RegularExpression);
    }

    private bool ExceedsMaxLength(string newText, bool paste) {
        if (MaxLength == 0) return false;

        return LengthOfModifiedText(newText, paste) > MaxLength;
    }

    private int LengthOfModifiedText(string newText, bool paste) {
        var countOfSelectedChars = AssociatedObject.SelectedText.Length;
        var caretIndex = AssociatedObject.CaretIndex;
        var text = AssociatedObject.Text;

        if (countOfSelectedChars > 0 || paste) {
            text = text.Remove(caretIndex, countOfSelectedChars);

            return text.Length + newText.Length;
        }
        
        return Keyboard.IsKeyToggled(Key.Insert) && caretIndex < text.Length ? text.Length : text.Length + newText.Length;
    }
}
