﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Elscrux.WPF.Models;
namespace Elscrux.WPF.Views.ComboBox; 

public partial class DirectoryChooser {
    public static readonly DependencyProperty DirectoriesProperty = DependencyProperty.Register(nameof(Directories), typeof(ObservableCollection<DirectoryItem>), typeof(DirectoryChooser), new PropertyMetadata(new ObservableCollection<DirectoryItem>()));
    public static readonly DependencyProperty SelectedDirectoryProperty = DependencyProperty.Register(nameof(SelectedDirectory), typeof(DirectoryItem), typeof(DirectoryChooser), new PropertyMetadata(default(DirectoryItem)));

    public string Header { get; set; } = string.Empty;
    
    public ObservableCollection<DirectoryItem> Directories {
        get => (ObservableCollection<DirectoryItem>)GetValue(DirectoriesProperty);
        set => SetValue(DirectoriesProperty, value);
    }
    
    public DirectoryItem? SelectedDirectory {
        get => (DirectoryItem?) GetValue(SelectedDirectoryProperty);
        set => SetValue(SelectedDirectoryProperty, value);
    }

    public event RoutedEventHandler? DirectorySelectionChanged;

    public DirectoryChooser() {
        InitializeComponent();

        DataContext = this;
    }

    private void DirectoryComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
        DirectorySelectionChanged?.Invoke(sender, e);
    }

    private void NewDirectory_OnClick(object sender, RoutedEventArgs e) {
        var fileDialog = new FolderBrowserDialog();
        if (fileDialog.ShowDialog() != DialogResult.OK) return;
        
        var directory = Directories.FirstOrDefault(d => d.Path == fileDialog.SelectedPath);
        
        //Add new directory if not found
        if (directory == null) {
            directory = new DirectoryItem(fileDialog.SelectedPath);
            Directories.Add(directory);
        }
        
        SelectedDirectory = directory;
    }
}