﻿using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Effects;
namespace Elscrux.WPF.Converters; 

public class BooleanToBlurConverter : IValueConverter {
    public static readonly BlurEffect Blur = new() { Radius = 10 };
    
    public object? Convert(object value, Type targetType, object parameter, CultureInfo culture) {
        return (bool)value ? Blur : null;
    }

    public object ConvertBack(object? value, Type targetType, object parameter, CultureInfo culture) {
        return value != null;
    }
}