using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace Elscrux.WPF.Models;

public class DirectoryItem : ReactiveObject, ISelectable {
    public DirectoryItem(string path) {
        Name = System.IO.Path.GetFileName(path);
        Path = path;
    }
    
    
    [Reactive]
    public bool IsSelected { get; set; }
    
    [Reactive]
    public string Name { get; protected set; }
    
    [Reactive]
    public string Path { get; protected set; }

    public override string ToString() => Name;
}
