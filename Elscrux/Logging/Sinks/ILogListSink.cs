﻿using System.Collections.ObjectModel;
using Serilog.Core;
namespace Elscrux.Logging.Sinks;

public interface ILogListSink : ILogEventSink {
    public ObservableCollection<ILogItem> LogItems { get; set; }
    
    public delegate void LogAddedHandler(ILogItem logItem);
    public event LogAddedHandler? OnLogAdded;
}