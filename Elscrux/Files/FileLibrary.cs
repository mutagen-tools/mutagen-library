﻿namespace Elscrux.Files; 

public static class FileLibrary {
    private const int KiloByte = 1024;
    private const int MegaByte = 1024 * 1024;
    private const int GigaByte = 1024 * 1024 * 1024;
    
    public static string GetFileSize(long size) {
        var doubleSize = decimal.ToDouble(size);
        return size switch {
            >= GigaByte => $"{doubleSize / GigaByte:F1} GB",
            >= MegaByte => $"{doubleSize / MegaByte:F1} MB",
            >= KiloByte => $"{doubleSize / KiloByte:F1} KB",
            _ => $"{size} Bytes"
        };
    }
}