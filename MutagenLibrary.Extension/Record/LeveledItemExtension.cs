﻿using Mutagen.Bethesda;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Cache;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim;
using Noggog;
namespace MutagenLibrary.Extension.Record;

public static class LeveledItemExtent {
    public static bool IsArmorSublist(this ILeveledItemGetter leveledItem, IArmorGetter baseArmor, int tier, string prefix = "") {
        //Example: EnchArmorElvenGauntletsOneHanded02, check for SublistEnchArmorElvenGauntlets02
        // base armor EditorID: ArmorElvenGauntlets needs to be included in SublistEnchArmorElvenGauntlets02
        // 02 of EnchArmorElvenGauntletsOneHanded02 needs to be included in SublistEnchArmorElvenGauntlets02
        return baseArmor.EditorID != null && IsArmorSublist(leveledItem.EditorID, baseArmor.EditorID.TrimStart(prefix), tier);
    }

    private static bool IsArmorSublist(string? leveledItemEditorId, string? armorEditorId, int tier) {
        return leveledItemEditorId != null && armorEditorId != null && leveledItemEditorId.Contains(armorEditorId, StringComparison.OrdinalIgnoreCase) && leveledItemEditorId.Contains(tier.ToString());
    }
    
    public static bool IsWeaponSublist(this ILeveledItemGetter leveledItem, IWeaponGetter weapon, string prefix = "") {
        //Example: EnchElvenSwordSoulTrap04, check for SublistEnchElvenSwordSoulTrap
        // EnchElvenSwordSoulTrap04 without tier needs to be included in SublistEnchElvenSwordSoulTrap
        return weapon.EditorID != null && IsWeaponSublist(leveledItem.EditorID, weapon.EditorID.TrimStart(prefix));
    }

    private static bool IsWeaponSublist(string? leveledItemEditorId, string? weaponEditorId) {
        return leveledItemEditorId != null && weaponEditorId != null && leveledItemEditorId.Contains(weaponEditorId[..^2], StringComparison.OrdinalIgnoreCase);
    }
    private static readonly char[] Digits = Enumerable.Range(0, 10).Select(i => i.ToString()[0]).ToArray();
    
    /// <summary>
    /// Adds an enchantable object to it's leveled list, and creates the leveled list,
    /// if it doesn't yet exist in the environment. The procedure to find the correct
    /// leveled list is fully determined by convention, so results may introduce more
    /// new leveled lists than necessary.
    ///
    /// For specifics how leveled lists armor found, see IsArmorSublist and IsWeaponSublist.
    ///  
    /// </summary>
    /// <param name="enchantable">enchantable to be added to a leveled list</param>
    /// <param name="linkCache">link cache to resolve references</param>
    /// <param name="mod">Mod to add overrides to</param>
    /// <param name="tier">Tier of the enchantable passed in</param>
    /// <param name="level">Level of  the enchantable in the leveled list</param>
    /// <param name="prefix">prefix of the mod to make sure it stays at the editor id start and sub-lists are properly detected by editor id</param>
    public static void AddToLeveledList<TMod, TModGetter>(this IEnchantableGetter enchantable, ILinkCache<TMod, TModGetter> linkCache, TMod mod, int tier, short level, string prefix)
        where TModGetter : class, IModGetter
        where TMod : class, TModGetter, IMod {
        if (enchantable.EditorID == null) return;

        ILeveledItem? list = null;
        switch (enchantable) {
            case IArmorGetter armor:
                var baseArmor = armor.TemplateArmor.TryResolve(linkCache);
                if (armor.EditorID == null || baseArmor?.EditorID == null) return;
                
                var basePrefixFreeEditorID = baseArmor.EditorID.TrimStart(prefix);

                //Find armor sublist that fits
                if (list == null) {
                    foreach (var leveledItem in linkCache.AllIdentifiers<ILeveledItemGetter>()) {
                        if (!IsArmorSublist(leveledItem.EditorID, basePrefixFreeEditorID, tier)) continue;

                        var context = linkCache.ResolveContext<ILeveledItem, ILeveledItemGetter>(leveledItem.FormKey);
                        
                        //Don't use any leveled lists that include armor with other template weapons
                        if (context.Record.Entries != null && !context.Record.Entries.All(x => {
                            var item = x.Data?.Reference.TryResolve(linkCache);
                            if (item == null) return true;

                            return item switch {
                                IArmorGetter armorEntry => armorEntry.TemplateArmor.Equals(armor.TemplateArmor),
                                _ => false
                            };
                        })) {
                            continue;
                        }
                        
                        list = context.GetOrAddAsOverride(mod);
                        break;
                    }
                }

                //Otherwise create new armor sublist
                list ??= new LeveledItem(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
                    ChanceNone = 0,
                    EditorID = $"{prefix}SublistEnch{basePrefixFreeEditorID}{tier:D2}",
                    Flags = LeveledItem.Flag.CalculateForEachItemInCount | LeveledItem.Flag.CalculateFromAllLevelsLessThanOrEqualPlayer
                };

                list.Entries ??= new ExtendedList<LeveledItemEntry>();
                list.Entries.Add(new LeveledItemEntry {
                    Data = new LeveledItemEntryData {
                        Count = 1,
                        Level = 1,
                        Reference = new FormLink<IItemGetter>(enchantable.FormKey)
                    }
                });

                break;
            case IWeaponGetter weapon:
                if (weapon.EditorID == null) return;
                
                var prefixFreeEditorID = enchantable.EditorID.TrimStart(prefix);

                //Find weapon sublist that fits
                if (list == null) {
                    foreach (var leveledItem in linkCache.AllIdentifiers<ILeveledItemGetter>()) {
                        if (!IsWeaponSublist(leveledItem.EditorID, prefixFreeEditorID)) continue;

                        var context = linkCache.ResolveContext<ILeveledItem, ILeveledItemGetter>(leveledItem.FormKey);
                        
                        //Don't use any leveled lists that include weapons with other template weapons
                        if (context.Record.Entries != null && !context.Record.Entries.All(x => {
                            var item = x.Data?.Reference.TryResolve(linkCache);
                            if (item == null) return true;

                            return item switch {
                                IWeaponGetter weaponEntry => weaponEntry.Template.Equals(weapon.Template),
                                _ => false
                            };
                        })) {
                            continue;
                        }
                        
                        list = context.GetOrAddAsOverride(mod);
                        break;
                    }
                }
            
                //Otherwise create new weapon sublist
                list ??= new LeveledItem(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
                    ChanceNone = 0,
                    EditorID = $"{prefix}Sublist{prefixFreeEditorID.TrimEnd(Digits)}"
                };
            
                list.Entries ??= new ExtendedList<LeveledItemEntry>();
                list.Entries.Add(new LeveledItemEntry {
                    Data = new LeveledItemEntryData {
                        Count = 1,
                        Level = level,
                        Reference = new FormLink<IItemGetter>(enchantable.FormKey)
                    }
                });
                break;
        }

        // var topLevelGroup = mod.GetTopLevelGroup<ILeveledItem>();
        if (list != null && !mod.GetTopLevelGroup<LeveledItem>().ContainsKey(list.FormKey)) {
            mod.GetTopLevelGroup<LeveledItem>().Add((list as LeveledItem)!);
        }
    }
}
