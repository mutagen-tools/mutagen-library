﻿using Mutagen.Bethesda.Skyrim;
namespace MutagenLibrary.Extension.Enum; 

public static class EnumConverter {
    public static Skill? ActorValueToSkill(ActorValue actorValue) {
        return actorValue switch {
            ActorValue.Destruction => Skill.Destruction,
            ActorValue.Conjuration => Skill.Conjuration,
            ActorValue.Alteration => Skill.Alteration,
            ActorValue.Illusion => Skill.Illusion,
            ActorValue.Restoration => Skill.Restoration,
            _ => null
        };
    }
}
