﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Elscrux.WPF.SF.Decorators.Behaviors;
using Microsoft.Xaml.Behaviors;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Syncfusion.UI.Xaml.Grid;
namespace Elscrux.WPF.SF.Views.DataGrids;

public abstract class ItemView : UserControl {
    public static RoutedUICommand GenericCommand { get; } = new("Some Command", "SomeCommand", typeof(ItemView));
}

public abstract class ItemView<T> : ItemView {
    public static readonly DependencyProperty ItemsProperty = DependencyProperty.Register(nameof(Items), typeof(ObservableCollection<T>), typeof(ItemView<T>), new PropertyMetadata(new ObservableCollection<T>()));
    
    public ObservableCollection<T> Items {
        get => (ObservableCollection<T>) GetValue(ItemsProperty);
        set => SetValue(ItemsProperty, value);
    }

    [Reactive] public bool Selectable { get; set; }
    [Reactive] public SfDataGrid DataGrid { get; set; } = new();

    protected void Setup(SfDataGrid dataGrid) {
        DataGrid = dataGrid;

        this.WhenAnyValue(x => x.Selectable)
            .Subscribe(_ => {
                if (Selectable) {
                    Interaction.GetBehaviors(DataGrid).Add(new SfDataGridSelectionBehavior());
                }
            });
    }

    public void AddCommands(List<CustomCommand> commands) {
        DataGrid.ContextMenu ??= new ContextMenu();
        
        foreach (var command in commands) {
            DataGrid.ContextMenu.Items.Add(new MenuItem {
                Header = command.Name,
                Command = GenericCommand,
                CommandBindings = { command.Binding }
            });
        }
    }

    public void AddCommands(List<MenuItem> menuItems) {
        DataGrid.ContextMenu ??= new ContextMenu();
        foreach (var menuItem in menuItems) {
            DataGrid.ContextMenu.Items.Add(menuItem);
        }
    }

    public void AddColumns(List<CustomDataGridTemplateColumn> columns) {
        foreach (var column in columns) {
            DataGrid.Columns.Add(new GridTemplateColumn {
                HeaderText = column.Header,
                CellTemplate = new DataTemplate(column.ElementType) { VisualTree = column.Element },
                MappingName = column.MappingName,
                IsReadOnly = column.IsReadOnly
            });
        }
    }

    public List<T> GetSelectedItems() {
        return DataGrid.SelectedItems.Cast<T>().ToList();
    }
    
    public void CanExecuteOneOrMultipleItemsSelected(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = DataGrid.SelectedItem != null;
    }
    
    public void CanExecuteOneItemSelected(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = DataGrid.SelectedItems.Count == 1;
    }
}

public class CustomDataGridTemplateColumn {
    public CustomDataGridTemplateColumn(FrameworkElementFactory element, Type elementType, string mappingName, string header = "", bool isReadOnly = true) {
        Element = element;
        ElementType = elementType;
        MappingName = mappingName;
        Header = header;
        IsReadOnly = isReadOnly;
    }

    public FrameworkElementFactory Element { get; init; }
    public Type ElementType { get; init; }
    public string MappingName { get; init; }
    public string Header { get; init; }
    public bool IsReadOnly { get; init; }
}

public class CustomCommand {
    public CustomCommand(string name, CommandBinding binding) {
        Name = name;
        Binding = binding;
    }

    public string Name { get; init; }
    public CommandBinding Binding { get; init; }
}