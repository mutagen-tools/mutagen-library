﻿using System.Windows.Forms;
using System.Windows.Input;
using Elscrux.WPF.Models;
namespace Elscrux.WPF.SF.Views.DataGrids; 

public partial class DirectoryView {
    public DirectoryView() {
        InitializeComponent();

        Setup(DirectoryGrid);
    }
    
    private void AddDirectory(object sender, ExecutedRoutedEventArgs e) {
        var fileDialog = new FolderBrowserDialog();
        if (fileDialog.ShowDialog() != DialogResult.OK) return;

        if (Items.Any(directory => directory.Path == fileDialog.SelectedPath)) return;
        
        var newDirectory = new DirectoryItem(fileDialog.SelectedPath);
        Items.Add(newDirectory);
    }
}