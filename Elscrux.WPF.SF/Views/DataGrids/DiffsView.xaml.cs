﻿using System.Reactive;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Elscrux.WPF.Models;
using Noggog;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
namespace Elscrux.WPF.SF.Views.DataGrids;

public partial class DiffsView {
    public static readonly DependencyProperty BaseDirectoryProperty = DependencyProperty.Register(nameof(BaseDirectory), typeof(string), typeof(DiffsView), new PropertyMetadata(string.Empty));
    public static readonly DependencyProperty CustomReplaceCommandProperty = DependencyProperty.Register(nameof(CustomReplaceCommand), typeof(ICommand), typeof(DiffsView), new PropertyMetadata(default(ICommand)));
    
    public string BaseDirectory {
        get => (string) GetValue(BaseDirectoryProperty);
        set => SetValue(BaseDirectoryProperty, value);
    }
    
    public ICommand? CustomReplaceCommand {
        get => (ICommand?) GetValue(CustomReplaceCommandProperty);
        set => SetValue(CustomReplaceCommandProperty, value);
    }
    
    public DiffReplacer ReplacementType { get; set; } = DiffReplacer.File;

    public DiffsView() {
        InitializeComponent();

        Setup(DiffsGrid);
    }

    private void CanReplace(object sender, CanExecuteRoutedEventArgs e) {
        if (DiffsGrid.SelectedItem == null) {
            e.CanExecute = false;
            return;
        }
        
        switch (ReplacementType) {
            case DiffReplacer.Custom:
                e.CanExecute = CustomReplaceCommand?.CanExecute(this) ?? false;
                break;
            case DiffReplacer.Directory:
            case DiffReplacer.File:
                e.CanExecute = !string.IsNullOrEmpty(BaseDirectory);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    
    }
    
    private void Replace(object sender, ExecutedRoutedEventArgs e) {
        switch (ReplacementType) {
            case DiffReplacer.Directory: {
                var fileDialog = new FolderBrowserDialog { Description = "Select new base directory" };
                if (fileDialog.ShowDialog() != DialogResult.OK) return;
    
                if (!fileDialog.SelectedPath.StartsWith(BaseDirectory)) {
                    MessageBox.Show("The directory needs to be inside the data directory.");
                    return;
                }
        
                var path = fileDialog.SelectedPath.Remove(0, BaseDirectory.Length).TrimStart('\\');
                foreach (var item in DiffsGrid.SelectedItems) {
                    if (item is not DiffItem diff) continue;
            
                    diff.SetNewBaseDirectory(path);
                }
                
                break;
            }
            case DiffReplacer.File:
            {
                var fileDialog = new OpenFileDialog { Multiselect  = false};
                if (fileDialog.ShowDialog() ?? false) return;
    
                if (!fileDialog.FileName.StartsWith(BaseDirectory)) {
                    MessageBox.Show("The file needs to be inside the data directory.");
                    return;
                }
            
                var path = fileDialog.FileName.Remove(0, BaseDirectory.Length).TrimStart('\\');
                foreach (var item in DiffsGrid.SelectedItems) {
                    if (item is not DiffItem diff) continue;
    
                    diff.SetNewFile(path);
                }
                
                break;
            }
            case DiffReplacer.Custom:
                CustomReplaceCommand?.Execute(Unit.Default);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        DiffsGrid.View.Refresh();
    }
    
    private void RemoveDiffs(object sender, ExecutedRoutedEventArgs e) {
        Items.Remove(DiffsGrid.SelectedItems.Cast<DiffItem>());
    }
    
    public enum DiffReplacer {
        Directory,
        File,
        Custom
    }
}