﻿using System.Windows.Controls;
using Syncfusion.Windows.Tools.Controls;
namespace Elscrux.WPF.SF.Extensions;

public static class DockingManagerExtension {
    public static TControl AddControl<TControl>(this DockingManager dockingManager, string header = "", DockSide dockSide = DockSide.Tabbed, DockState dockState = DockState.Document, double? size = null)
        where TControl : UserControl, new() {
        var control = new TControl();
        control.SetValue(DockingManager.HeaderProperty, header);
        control.SetValue(DockingManager.SideInDockedModeProperty, dockSide);
        control.SetValue(DockingManager.StateProperty, dockState);

        if (size != null) {
            control.SetValue(dockState switch {
                    DockState.Dock when dockSide is DockSide.Left or DockSide.Right => DockingManager.DesiredWidthInDockedModeProperty,
                    DockState.Dock when dockSide is DockSide.Bottom or DockSide.Top => DockingManager.DesiredHeightInDockedModeProperty,
                    DockState.Float when dockSide is DockSide.Left or DockSide.Right => DockingManager.DesiredWidthInFloatingModeProperty,
                    DockState.Float when dockSide is DockSide.Bottom or DockSide.Top => DockingManager.DesiredHeightInFloatingModeProperty,
                }, size);
        }
        
        dockingManager.Children.Add(control);

        return control;
    }
    
    public static TControl AddControl<TControl>(this DockingManager dockingManager, TControl control, string header = "", DockSide dockSide = DockSide.Tabbed, DockState dockState = DockState.Document, double? size = null)
        where TControl : UserControl {
        control.SetValue(DockingManager.HeaderProperty, header);
        control.SetValue(DockingManager.SideInDockedModeProperty, dockSide);
        control.SetValue(DockingManager.StateProperty, dockState);

        if (size != null) {
            control.SetValue(dockState switch {
                DockState.Dock when dockSide is DockSide.Left or DockSide.Right => DockingManager.DesiredWidthInDockedModeProperty,
                DockState.Dock when dockSide is DockSide.Bottom or DockSide.Top => DockingManager.DesiredHeightInDockedModeProperty,
                DockState.Float when dockSide is DockSide.Left or DockSide.Right => DockingManager.DesiredWidthInFloatingModeProperty,
                DockState.Float when dockSide is DockSide.Bottom or DockSide.Top => DockingManager.DesiredHeightInFloatingModeProperty,
            }, size);
        }
        
        dockingManager.Children.Add(control);

        return control;
    }
}
