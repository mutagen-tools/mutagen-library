﻿using System.Windows;
using System.Windows.Media;
using Microsoft.Win32;
using Syncfusion.SfSkinManager;
using Syncfusion.Themes.MaterialDark.WPF;
using Syncfusion.Themes.MaterialLight.WPF;
namespace Elscrux.WPF.SF.Extensions; 

public static class ThemeExtension {
    private const string RegistryKeyPath = @"Software\Microsoft\Windows\CurrentVersion\Themes\Personalize";
    private const string RegistryValueName = "AppsUseLightTheme";
    private const string DarkThemeName = "MaterialDark";
    private const string LightThemeName = "MaterialLight";
    
    private enum WindowsTheme { Light, Dark }

    private static WindowsTheme GetTheme() {
        using var key = Registry.CurrentUser.OpenSubKey(RegistryKeyPath);
        var registryValueObject = key?.GetValue(RegistryValueName);
        if (registryValueObject == null) return WindowsTheme.Light;

        var registryValue = (int) registryValueObject;
        return registryValue == 1 ? WindowsTheme.Light : WindowsTheme.Dark;
    }
    
    public static void UpdateTheme(this DependencyObject dependencyObject) {
        if (GetTheme() == WindowsTheme.Dark) {
            var darkThemeSettings = new MaterialDarkThemeSettings {
                PrimaryBackground = new SolidColorBrush(SystemParameters.WindowGlassColor)
            };
            SfSkinManager.RegisterThemeSettings(DarkThemeName, darkThemeSettings);
            SfSkinManager.SetTheme(dependencyObject, new Theme(DarkThemeName));
        } else {
            var lightThemeSettings = new MaterialLightThemeSettings {
                PrimaryBackground = new SolidColorBrush(SystemParameters.WindowGlassColor)
            };
            SfSkinManager.RegisterThemeSettings(LightThemeName, lightThemeSettings);
            SfSkinManager.SetTheme(dependencyObject, new Theme(LightThemeName));
        }
    }
}
