﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;
using Microsoft.Xaml.Behaviors.Core;
using Noggog;
using ReactiveUI;
using Syncfusion.UI.Xaml.Grid;
using EventTrigger = Microsoft.Xaml.Behaviors.EventTrigger;
namespace Elscrux.WPF.SF.Decorators.Behaviors;

public class SfDataGridSelectionBehavior : Behavior<SfDataGrid> {
    public static readonly DependencyProperty AllCheckedProperty = DependencyProperty.Register(nameof(AllChecked), typeof(bool?), typeof(SfDataGridSelectionBehavior), new FrameworkPropertyMetadata(false));
    public static readonly DependencyProperty SelectionGuardProperty = DependencyProperty.Register(nameof(SelectionGuard), typeof(Func<ISelectable, bool>), typeof(SfDataGridSelectionBehavior), new FrameworkPropertyMetadata(new Func<ISelectable, bool>(_ => true)));
    
    private GridTemplateColumn _column = null!;
    private bool _isProcessing;

    public string? EnabledMapping { get; set; }

    public bool AddColumn { get; set; } = true;
    public bool AddCommands { get; set; } = true;
    public bool AddKeyBind { get; set; } = true;

    public Key KeyBindKey { get; set; } = Key.Space;
    public string KeyBindEventName { get; set; } = "KeyUp";
    
    public bool? AllChecked {
        get => (bool?) GetValue(AllCheckedProperty);
        set => SetValue(AllCheckedProperty, value);
    }
    
    public Func<ISelectable, bool> SelectionGuard {
        get => (Func<ISelectable, bool>) GetValue(SelectionGuardProperty);
        set => SetValue(SelectionGuardProperty, value);
    }

    protected override void OnAttached() {
        base.OnAttached();

        if (AddColumn) AddSelectionColumn();
        if (AddCommands) AddSelectionMenu();
        if (AddKeyBind) AddKeyBindings();
    }
    

    private void AddSelectionColumn() {
        var cellCheckBox = new FrameworkElementFactory(typeof(CheckBox));
        cellCheckBox.AddHandler(ToggleButton.CheckedEvent, new RoutedEventHandler((_, _) => UpdateAllChecked()));
        cellCheckBox.AddHandler(ToggleButton.UncheckedEvent, new RoutedEventHandler((_, _) => UpdateAllChecked()));
        cellCheckBox.SetValue(FocusManagerHelper.FocusedElementProperty, true);
        cellCheckBox.SetValue(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Center);
        cellCheckBox.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Center);
        cellCheckBox.SetBinding(ToggleButton.IsCheckedProperty, new Binding(nameof(ISelectable.IsSelected)));
        if (EnabledMapping != null) cellCheckBox.SetBinding(UIElement.IsEnabledProperty, new Binding(EnabledMapping));

        var columnCheckBox = new FrameworkElementFactory(typeof(CheckBox));
        columnCheckBox.AddHandler(ToggleButton.CheckedEvent, new RoutedEventHandler((_, _) => SelectAllItems()));
        columnCheckBox.AddHandler(ToggleButton.UncheckedEvent, new RoutedEventHandler((_, _) => SelectAllItems(false)));
        columnCheckBox.SetBinding(ToggleButton.IsCheckedProperty, new Binding(nameof(AllChecked)) { Source = this });

        _column = new GridTemplateColumn {
            HeaderTemplate = new DataTemplate(typeof(CheckBox)) { VisualTree = columnCheckBox },
            CellTemplate = new DataTemplate(typeof(CheckBox)) { VisualTree = cellCheckBox },
            AllowFiltering = false,
            AllowDragging = false,
            AllowSorting = false,
            AllowGrouping = false,
            AllowResizing = false,
            AllowEditing = true,
            Width = 30
        };

        AssociatedObject.Columns.Insert(0, _column);
    }

    private void AddSelectionMenu() {
        AssociatedObject.ContextMenu ??= new ContextMenu();

        var selectAllMenu = new MenuItem {
            Header = "Select All",
            Command = ReactiveCommand.Create(() => SelectDynamic())
        };
        AssociatedObject.ContextMenu.Items.Insert(0, selectAllMenu);

        var invertMenu = new MenuItem {
            Header = "Invert",
            Command = ReactiveCommand.Create(InvertAll)
        };
        AssociatedObject.ContextMenu.Items.Insert(0, invertMenu);
    }

    private void AddKeyBindings() {
        var eventTrigger = new EventTrigger {
            EventName = KeyBindEventName,
            Actions = {
                new InvokeCommandAction {
                    Command = new ActionCommand(o => {
                        if (o is KeyEventArgs args && args.Key == KeyBindKey) {
                            ToggleSelection();
                        }
                    }),
                    PassEventArgsToCommand = true
                }
            }
        };

        Interaction.GetTriggers(AssociatedObject).Add(eventTrigger);
    }

    private void TryProcess(Action action) {
        if (_isProcessing) return;

        _isProcessing = true;
        action.Invoke();
        _isProcessing = false;
    }

    private void UpdateAllChecked() {
        TryProcess(() => {
            if (AssociatedObject.View.Records.All(record => (record.Data as ISelectable)!.IsSelected)) {
                AllChecked = true;
            } else if (AssociatedObject.View.Records.Any(record => (record.Data as ISelectable)!.IsSelected)) {
                AllChecked = null;
            } else {
                AllChecked = false;
            }
        });
    }
    
    private void SelectSelectedItems(bool newState = true) {
        TryProcess(() => {
            _isProcessing = true;
            foreach (var selectedItem in AssociatedObject.SelectedItems) {
                var selectable = (ISelectable) selectedItem;
                selectable.IsSelected = newState && SelectionGuard.Invoke(selectable);
            }
        });
        
        UpdateAllChecked();
    }

    private void SelectAllItems(bool newState = true) {
        TryProcess(() => {
            foreach (var item in AssociatedObject.View.Records.NotNull()) {
                var selectable = (ISelectable) item.Data;
                selectable.IsSelected = newState && SelectionGuard.Invoke(selectable);
                AllChecked = newState;
            }
        });
    }

    private void SelectDynamic(bool newState = true) {
        if (AssociatedObject.SelectedItems.Count > 1) {
            //Only select records in selection if multiple are selected
            SelectSelectedItems(newState);
        } else {
            //Otherwise select all records
            SelectAllItems(newState);
        }
    }

    private void ToggleSelection() {
        TryProcess(() => {
            _isProcessing = true;
            var newStatus = !AssociatedObject.SelectedItems
                .Cast<ISelectable>()
                .All(selectable => selectable.IsSelected);

            AssociatedObject.SelectedItems
                .Cast<ISelectable>()
                .ForEach(selectable => selectable.IsSelected = newStatus && SelectionGuard.Invoke(selectable));
        });
        
        UpdateAllChecked();
    }

    private void InvertAll() {
        TryProcess(() => {
            foreach (var item in AssociatedObject.View.Records.NotNull()) {
                if (item.Data is ISelectable selectable) {
                    selectable.IsSelected = !selectable.IsSelected && SelectionGuard.Invoke(selectable);
                }
            }
        });
        
        UpdateAllChecked();
    }
}
