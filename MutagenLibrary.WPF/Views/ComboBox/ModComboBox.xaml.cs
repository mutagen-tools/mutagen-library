﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Records;
using MutagenLibrary.WPF.Models;
using Noggog;
namespace MutagenLibrary.WPF.Views.ComboBox; 

public partial class ModComboBox {
    public static readonly DependencyProperty SelectedModItemProperty = DependencyProperty.Register(nameof(SelectedModItem), typeof(ModItem), typeof(ModComboBox), new PropertyMetadata(default(ModItem)));
    public static readonly DependencyProperty ModsProperty = DependencyProperty.Register(nameof(Mods), typeof(ObservableCollection<ModItem>), typeof(ModComboBox), new PropertyMetadata(new ObservableCollection<ModItem>()));
    
    public bool CanSelectNewPlugin { get; set; }
    
    public string Header { get; set; } = string.Empty;

    private string _headerToolTip = string.Empty;
    public string HeaderToolTip {
        get => _headerToolTip;
        set {
            _headerToolTip = value;
            TextBlock.ToolTip = value.IsNullOrWhitespace() ? null : value;
        }
    }
    
    public ModItem? SelectedModItem {
        get => (ModItem?) GetValue(SelectedModItemProperty);
        set => SetValue(SelectedModItemProperty, value);
    }
    
    public ObservableCollection<ModItem> Mods {
        get => (ObservableCollection<ModItem>)GetValue(ModsProperty);
        set {
            if (CanSelectNewPlugin) value.Add(new ModItem(ModItem.NewMod));
            SetValue(ModsProperty, value);
        }
    }

    public event SelectionChangedEventHandler? ModSelectionChanged;
    
    public ModComboBox() {
        InitializeComponent();

        DataContext = this;
    }

    public IModGetter? GetSelectedMod(IGameEnvironment environment) {
        return SelectedModItem == null || SelectedModItem.ModKey == ModKey.Null ? null : environment.LoadOrder.First(mod => mod.Key == SelectedModItem.ModKey).Value.Mod;
    }
    
    private void ComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
        ModSelectionChanged?.Invoke(sender, e);
    }
}