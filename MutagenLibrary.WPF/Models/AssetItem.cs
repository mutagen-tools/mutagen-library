﻿using MutagenLibrary.Core.Assets;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenLibrary.WPF.Models;

public class AssetItem : ReactiveObject, ISelectable {
    public AssetItem(string path) {
        Name = System.IO.Path.GetFileName(path);
        Path = path;
        AssetType = AssetTypeLibrary.GetAssetType(path);
    }
    
    [Reactive]
    public bool IsSelected { get; set; }
    
    [Reactive]
    public string Name { get; protected set; }
    
    [Reactive]
    public string Path { get; protected set; }
    
    [Reactive]
    public AssetType AssetType { get; protected set; }

    public override string ToString() => Name;
}
