﻿using Mutagen.Bethesda;
using Mutagen.Bethesda.Environments.DI;
using Mutagen.Bethesda.Installs;
namespace MutagenLibrary.Core.Plugins;

public interface ISimpleEnvironmentContext {
    public IGameReleaseContext GameReleaseContext { get; }
    public IDataDirectoryProvider DataDirectoryProvider  { get; }
}

public class SimpleEnvironmentContext : ISimpleEnvironmentContext {
    public IGameReleaseContext GameReleaseContext { get; }
    public IDataDirectoryProvider DataDirectoryProvider  { get; }
    
    private SimpleEnvironmentContext(
        IGameReleaseContext gameReleaseContext,
        IDataDirectoryProvider dataDirectoryProvider) {
        GameReleaseContext = gameReleaseContext;
        DataDirectoryProvider = dataDirectoryProvider;
    }

    public static SimpleEnvironmentContext Build(GameRelease gameRelease) {
        return new SimpleEnvironmentContext(
            new GameReleaseInjection(gameRelease),
            new DataDirectoryInjection(GameLocations.GetDataFolder(gameRelease)));
    }
}
