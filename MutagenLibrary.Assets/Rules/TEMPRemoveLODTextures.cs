﻿using System.Text.RegularExpressions;
using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Rules;

public class TempRemoveLODTextures : AssetRule {
    private const string TerrainPath = "textures\\terrain\\";
    private static readonly Regex WaterRegex = new(@"textures\\water\\.+\\flow.-?[0-9]+.-?[0-9]+.dds");

    protected override List<AssetType> ValidAssetTypes => new() { AssetType.Texture };
    protected override int Priority => 900;

    protected override IEnumerable<string> ApplyRule(string path) {
        var waterMatch = WaterRegex.Match(path);
        return path.StartsWith(TerrainPath, StringComparison.OrdinalIgnoreCase) || waterMatch.Success ? new List<string>() : new List<string> { path };
    }
}