﻿using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Rules;

public class TEMPRemoveInterfaceTextures : AssetRule {
    private const string TerrainPath = "interface\\";

    protected override List<AssetType> ValidAssetTypes => new() { AssetType.Texture };
    protected override int Priority => 900;

    protected override IEnumerable<string> ApplyRule(string path) {
        return path.StartsWith(TerrainPath, StringComparison.OrdinalIgnoreCase) ? new List<string>() : new List<string> { path };
    }
}