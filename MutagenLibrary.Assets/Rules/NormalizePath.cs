﻿using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Rules;

public class NormalizePath : AssetRule {
    protected override List<AssetType> ValidAssetTypes => new() { AssetType.None };
    protected override int Priority => 1023;

    protected override IEnumerable<string> ApplyRule(string path) {
        yield return path.Replace('/', '\\');
    }
}