﻿using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Rules; 

public class AlwaysUseWAVSoundExtension : AssetRule {
    private const string WAVExtension = ".wav";

    protected override List<AssetType> ValidAssetTypes => new() { AssetType.Sound };
    protected override int Priority => 512;

    protected override IEnumerable<string> ApplyRule(string path) {
        if (!path.EndsWith(WAVExtension, StringComparison.OrdinalIgnoreCase)) {
            path = Path.ChangeExtension(path, WAVExtension);
        }

        yield return path;
    }
}