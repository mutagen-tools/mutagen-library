﻿using System.Collections.Immutable;
using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Managers;

public class AssetManagerList {
    private readonly List<AssetManager> _assetManagers = new();

    public void Add(AssetManager assetManager) {
        _assetManagers.Add(assetManager);
    }

    public void AddRange(IEnumerable<AssetManager> assetManagers) {
        foreach (var assetManager in assetManagers) {
            _assetManagers.Add(assetManager);
        }
    }

    public void SetTo(IEnumerable<AssetManager> assetManagers) {
        Clear();
        AddRange(assetManagers);
    }

    public void Remove(AssetManager assetManager) {
        _assetManagers.Remove(assetManager);
    }

    public void Clear() {
        _assetManagers.Clear();
    }

    public bool HasAsset(Asset asset, AssetType assetType) {
        return _assetManagers.Any(manager => manager.HasAsset(asset, assetType));
    }

    public List<Asset> GetMissingAssets(AssetManagerList assetManagerList, IEnumerable<AssetType> assetTypes) {
        var missingAssets = new HashSet<Asset>();
        foreach (var asset in from assetManager in _assetManagers
                 from assetType in assetTypes
                 from asset in assetManager.GetRequiredAssets(assetType)
                 where !assetManagerList.HasAsset(asset, assetType)
                 select asset)
            missingAssets.Add(asset);

        return new List<Asset>(missingAssets.ToImmutableSortedSet());
    }

    public List<Asset> GetAllAssets(IEnumerable<AssetType> assetTypes) {
        var assets = new HashSet<Asset>();

        foreach (var asset in from assetManager in _assetManagers from assetType in assetTypes from asset in assetManager.GetAssets(assetType) select asset) assets.Add(asset);

        return new List<Asset>(assets.ToImmutableSortedSet());
    }

    public IEnumerable<AssetManager> GetUsers(Asset asset, AssetType assetType) {
        return _assetManagers.Where(assetManager => assetManager.HasAsset(asset, assetType));
    }

    public IEnumerable<string> GetUsages(Asset asset) {
        var usages = new List<string>();
        var assetType = AssetTypeLibrary.GetAssetType(asset.Path);

        foreach (var usage in from assetManager in _assetManagers from usage in assetManager.GetUsages(asset, assetType) where !usages.Contains(usage) select usage) usages.Add(usage);

        return usages;
    }
}