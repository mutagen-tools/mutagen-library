﻿using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.VisualBasic;
using MutagenLibrary.WPF.SF.Json.Editor;
using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Presets;

public abstract class PresetsConfigurator : BasicPresetsConfigurator {
    public static readonly DependencyProperty FileNamesProperty = DependencyProperty.Register(nameof(FileNames), typeof(ObservableCollection<string>), typeof(BasicPresetsConfigurator), new PropertyMetadata(default(ObservableCollection<string>)));
    public static readonly DependencyProperty SelectedFileNameProperty = DependencyProperty.Register(nameof(SelectedFileName), typeof(string), typeof(BasicPresetsConfigurator), new PropertyMetadata(default(string)));
    public static readonly DependencyProperty SelectedFilePathProperty = DependencyProperty.Register(nameof(SelectedFilePath), typeof(string), typeof(BasicPresetsConfigurator), new PropertyMetadata(default(string)));
    public static readonly DependencyProperty AddCommandProperty = DependencyProperty.Register(nameof(AddCommand), typeof(ICommand), typeof(BasicPresetsConfigurator), new PropertyMetadata(default(ICommand)));
    public static readonly DependencyProperty RemoveCommandProperty = DependencyProperty.Register(nameof(RemoveCommand), typeof(ICommand), typeof(BasicPresetsConfigurator), new PropertyMetadata(default(ICommand)));
    
    public ICommand AddCommand {
        get => (ICommand) GetValue(AddCommandProperty);
        set => SetValue(AddCommandProperty, value);
    }
    public ICommand RemoveCommand {
        get => (ICommand) GetValue(RemoveCommandProperty);
        set => SetValue(RemoveCommandProperty, value);
    }

    public ObservableCollection<string> FileNames {
        get => (ObservableCollection<string>) GetValue(FileNamesProperty);
        set => SetValue(FileNamesProperty, value);
    }
    
    public string SelectedFileName {
        get => (string) GetValue(SelectedFileNameProperty);
        set => SetValue(SelectedFileNameProperty, value);
    }
    public string SelectedFilePath {
        get => (string) GetValue(SelectedFilePathProperty);
        set => SetValue(SelectedFilePathProperty, value);
    }

    public PresetsConfigurator() {
        SetResourceReference(StyleProperty, "ConfiguratorStyle");
    }
}

public abstract class PresetsConfigurator<TPresetData, TPresetManager, TDataEditor> : PresetsConfigurator
    where TPresetData : class, new()
    where TPresetManager : PresetManager<TPresetData, TPresetManager>
    where TDataEditor : JsonEditor, new() {
    
    private readonly TPresetManager _presetManager = (typeof(TPresetManager).GetField("Instance", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy)!.GetValue(null) as Lazy<TPresetManager>)!.Value;
    
    public TDataEditor Editor { get; set; } = new();
    
    public PresetsConfigurator() {
        SelectedFilePath = _presetManager.ActivePresetFile;
        SelectedFileName = _presetManager.DefaultName;
        Editor.DataContext = this;
        Editor.SetBinding(JsonEditor.JsonFileProperty, new Binding(nameof(SelectedFilePath)));
        Editor.SetBinding(JsonEditor.ReadOnlyProperty, new Binding(nameof(IsReadOnly)));

        AddCommand = ReactiveCommand.Create(() => {
            var newPresetName = Interaction.InputBox("Enter a Preset Name", "New Preset");
            if (_presetManager.HasPreset(newPresetName)) return;

            _presetManager.SetPreset(newPresetName);
            SelectedFileName = newPresetName;
        });

        RemoveCommand = ReactiveCommand.Create(
            canExecute: this.WhenAnyValue(x => x.IsReadOnly)
                .Select(x => !x),
            execute: () => {
                _presetManager.RemoveActivePreset();
                SelectedFileName = _presetManager.DefaultName;
            }
        );

        this.WhenAnyValue(x => x.SelectedFileName)
            .Throttle(TimeSpan.FromMilliseconds(250), RxApp.MainThreadScheduler)
            .Subscribe(_ => {
                _presetManager.SetPreset(SelectedFileName);
                IsReadOnly = _presetManager.IsDefaultPreset();
                SelectedFilePath = _presetManager.ActivePresetFile;
            });

        this.WhenAnyValue(x => x._presetManager.PresetFileNames)
            .BindTo(this, x => x.FileNames);
    }
}