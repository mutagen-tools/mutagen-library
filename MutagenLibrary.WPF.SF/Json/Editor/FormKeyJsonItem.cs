﻿using System.Reflection;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Cache;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.WPF.SF.Json.Editor.View;
using Newtonsoft.Json.Linq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public class FormKeyJsonItem : JsonItem<FormKey> {
    private static readonly IGameEnvironment Environment = GameEnvironment.Typical.Skyrim(SkyrimRelease.SkyrimSE, LinkCachePreferences.OnlyIdentifiers());
    public static ILinkCache LinkCache { get; } = Environment.LinkCache;
    
    public IEnumerable<Type> ScopedTypes { get; set; }
    
    [Reactive]
    public string? EditorID { get; set; }
    
    public FormKeyJsonItem(Type type, JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) : base(typeof(FormKey), token, parent, memberInfo) {
        Value = FormKey.Factory(token.ToObject<string>());
        ScopedTypes = type.GenericTypeArguments;
        EditorID = LinkCache.TryResolveIdentifier(Value, ScopedTypes, out var editorId) ? editorId : FormKey.NullStr;
        View = new FormKeyJsonItemView(this);

        this.WhenAnyValue(x => x.Value)
            .Subscribe(_ => {
                if (JsonValue == null) return;

                if (Token is JValue value) value.Value = JsonValue;
            });
    }
    
    public override object? JsonValue => Value.ToString();
}
