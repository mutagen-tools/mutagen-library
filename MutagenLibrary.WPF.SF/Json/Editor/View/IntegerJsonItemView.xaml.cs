﻿using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor.View; 

public class IntegerJsonItemViewBase : ReactiveUserControl<IntegerJsonItem> { }

public partial class IntegerJsonItemView {
    public IntegerJsonItemView(IntegerJsonItem integerJsonItem) {
        InitializeComponent();

        DataContext = ViewModel = integerJsonItem;
    }
}

